Привет, ты попал сюда потому что хочешь установить специальную праздничую сборку майнкрафта! 

Выбери какой у тебя майнкрафт: 
 - [Лицензия](https://gitlab.com/heesa/heesacinema-cdn/-/blob/main/minecraft-launcher.md)
 - [MultiMC](https://gitlab.com/heesa/heesacinema-cdn/-/blob/main/mmc-launcher.md)
 - [Без лицензии](https://gitlab.com/heesa/heesacinema-cdn/-/blob/main/mmc-offline-launcher.md)

Дополнительные инструкции:
 - [Для TL-Launcher](https://gitlab.com/heesa/heesacinema-cdn/-/blob/main/tl-launcher.md)
 - [Ручная установка на Официальный лаунчер](https://gitlab.com/heesa/heesacinema-cdn/-/blob/main/manual-install.md)

Удачи, надеюсь ты справишься С: