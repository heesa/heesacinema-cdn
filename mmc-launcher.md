# MultiMC 

0. Скачай и установи Java 16 или 17. Если ещё нет, то взять её можно [отсюда](https://adoptium.net/releases.html?variant=openjdk17&jvmVariant=hotspot)
1. Скачай сборку для MultiMC [СКАЧАТЬ](https://gitlab.com/heesa/heesacinema-cdn/-/blob/main/builds/mmc/heesacinema.zip)
2. Отрой MultiMC если ещё не сделал этого.
3. Просто перетащи архив в окно MultiMC.
4. В открывшемся окне нажми **ОК**
5. Запускай, балдей!

По всем ошибкам и не поняткам можно писать мне в дискорд, разберёмся.