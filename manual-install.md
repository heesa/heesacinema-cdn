# Ручная установка

0. Закрыть лаунчер.
1. [Скачай установщик Fabric](https://maven.fabricmc.net/net/fabricmc/fabric-installer/0.10.2/fabric-installer-0.10.2.exe), запусти его. Выбери версию Minecraft: `1.16.5`, версию загрузчика можно оставить без изменений. Нажать кнопку `Установить`.
2. Запустить лаунчер майнкрафта, перейти в: `Установки` - `Новая установка`.
3. Заполняем поля: 
 - Название: любое;
 - Версия: (выбрать из списка) `release fabric-loader-0.XX.XX-1.16.5` _(вместо xx.xx могут быть любые цифры)_;
 - Папка игры: Любой удобный путь. Например `C:\heesacinema`;
 - **ВАЖНО!** Кликнуть `Больше настроек`, строка `Параметры JVM`: в самый конец строки, отступив пробел, вставить этот ключ: `-Djavax.net.ssl.trustStoreType=WINDOWS-ROOT`
 - Нажать кнопку `Создать`;
4. [Скачай архив со сборокой](https://gitlab.com/heesa/heesacinema-cdn/-/blob/main/builds/heesacinema-win64.zip), Распакуй его в папку которую ты указал в пункте 3. Например в `C:\heesacinema`.
5. Можно запускать игру и проверять.

Сложно? Ну так воспользуйся [профайлером](https://gitlab.com/heesa/heesacinema-cdn/-/blob/main/minecraft-launcher.md), для кого я это всё делал.